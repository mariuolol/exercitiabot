import sqlite3

class DBHelper:
    def __init__(self, dbname="latcondb3.sqlite"):
        self.dbname = dbname
        self.conn = sqlite3.connect(dbname)

    def setup(self):

        print("Creating Table")

        tblstmt = "CREATE TABLE IF NOT EXISTS items (keys text, answer1 text, answer2 text, owner text)"
        keysidx = "CREATE INDEX IF NOT EXISTS itemIndex ON items (keys ASC)"
        ans1idx = "CREATE INDEX IF NOT EXISTS itemIndex ON items (answer1 ASC)"
        ans2idx = "CREATE INDEX IF NOT EXISTS itemIndex ON items(answer2 ASC)"
        ownidx = "CREATE INDEX IF NOT EXISTS ownIndex ON items (owner ASC)"

        self.conn.execute(tblstmt)
        self.conn.execute(keysidx)
        self.conn.execute(ans1idx)
        self.conn.execute(ans2idx)
        self.conn.execute(ownidx)

        self.conn.commit()

    def add_item(self, key, item1_text, item2_text, owner):
        stmt = "INSERT INTO items (keys, answer1, answer2, owner) VALUES (?, ?, ?, ?)"
        args = (key, item1_text, item2_text, owner)
        self.conn.execute(stmt, args)
        self.conn.commit()
        print("Inserted items in table")

    def delete_item(self, item_text, owner):
        stmt = "DELETE FROM items WHERE keys = (?) AND owner = (?)"
        args = (item_text, owner )
        self.conn.execute(stmt, args)
        self.conn.commit()

    def delete_items(self, key, item1, item2, owner):
        stmt = "DELETE FROM items WHERE keys = (?) AND answer1 = (?) AND answer2 = (?) AND owner = (?)"
        args = (key, item1, item2, owner)
        self.conn.execute(stmt, args)
        self.conn.commit()
        print("Deleted items from table")

    def get_keys(self, owner):
        stmt = "SELECT keys FROM items WHERE owner = (?)"
        args = (owner,  )
        return [x[0] for x in self.conn.execute(stmt, args)]

    def get_right_key(self, item_text, owner):
        stmt = "SELECT keys FROM items WHERE answer1 = (?) AND owner = (?) OR answer2 = (?) AND owner = (?)"
        args = (item_text, owner, item_text, owner)
        return [x[0] for x in self.conn.execute(stmt, args)]

    def get_answer1(self, owner):
        stmt = "SELECT answer1 FROM items WHERE owner = (?)"
        args = (owner, )
        return [x[0] for x in self.conn.execute(stmt, args)]
    def get_answer2(self, owner):
        stmt = "SELECT answer2 FROM items WHERE owner = (?)"
        args = (owner, )
        return [x[0] for x in self.conn.execute(stmt, args)]


"""class DBSbagliate:
    def __init__(self, dbname="lat4dbs.sqlite"):
        self.dbname = dbname
        self.conn = sqlite3.connect(dbname)

    def setup(self):
        print("creating table")
        tblstmt = "CREATE TABLE IF NOT EXISTS items (description text, owner text)"
        itemidx = "CREATE INDEX IF NOT EXISTS itemIndex ON items (description ASC)"
        ownidx = "CREATE INDEX IF NOT EXISTS ownIndex ON items (owner ASC)"
        self.conn.execute(tblstmt)
        self.conn.execute(itemidx)
        self.conn.execute(ownidx)
        self.conn.commit()

    def add_item(self, item_text, owner):
        stmt = "INSERT INTO items (description, owner) VALUES (?, ?)"
        args = (item_text, owner)
        self.conn.execute(stmt, args)
        self.conn.commit()

    def delete_item(self, item_text, owner):
        stmt = "DELETE FROM items WHERE description = (?) AND owner = (?)"
        args = (item_text, owner )
        self.conn.execute(stmt, args)
        self.conn.commit()

    def get_items(self, owner):
        stmt = "SELECT description FROM items WHERE owner = (?)"
        args = (owner, )
        return [x[0] for x in self.conn.execute(stmt, args)]"""
