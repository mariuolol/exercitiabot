import json
import requests
import time
import urllib
import random
from random import randint

from database import DBHelper
#from dbbot4 import DBSbagliate
db = DBHelper()
#dbs = DBSbagliate()

from dizverbi import verbi1, verbi23, verbi4, verbim
from diznomi import nomi_maschili, nomi_femminili, nomi_neutri
from dizagg import agg1, agg2

lista_diz = [verbi1, verbi23, verbi4, verbim, nomi_maschili, nomi_femminili, nomi_neutri, agg1, agg2]
lista_verbi = [verbi1, verbi23, verbi4, verbim]
lista_nomi = [nomi_maschili, nomi_femminili, nomi_neutri]
lista_agg = [agg1, agg2]

lista_trad = []
for dizionario in lista_diz:
    for key in dizionario.keys():
        lista_trad.append(key)

TOKEN = "546584742:AAHb2zq-qOlBXlVELpQFpN7vDBA2m8xztBA"
URL = "https://api.telegram.org/bot{}/".format(TOKEN)

def get_url(url):
    response = requests.get(url)
    content = response.content.decode("utf-8")
    return content

def get_json_from_url(url):
    content = get_url(url)
    js = json.loads(content)
    return js

def get_updates(offset=None):
    url = URL + "getUpdates?timeout=100"
    if offset:
        url += "&offset={}".format(offset)
    js = get_json_from_url(url)
    return js

def get_last_update_id(updates):
    update_ids = []
    for update in updates["result"]:
        update_ids.append(int(update["update_id"]))
    return max(update_ids)

def crea_vocabolo(lista_diz, chat):
    diz = lista_diz[random.randint(0, ((len(lista_diz))-1))]
    verbo = random.choice(list(diz.keys()))
    risposta = diz[verbo]
    print(verbo, risposta)

    if (len(risposta)) == 2:
        risposta = str(risposta)
        risposta = ((risposta).replace("[", "")).replace("]", "").replace(" ", "")
        risp1, risp2 = risposta.split(",")
        risp1 = risp1[1:-1]
        risp2 = risp2[1:-1]
        print("Answer1: ", risp1, "Answer2:", risp2)
        db.add_item(verbo, risp1, risp2, chat)
    else:
        risposta = str(risposta)
        risposta = ((risposta).replace("[", "")).replace("]", "")
        db.add_item(verbo, risposta, "None", chat)

    message = "Traduci `{}`".format(verbo)
    send_message(message, chat)

    #dbs.add_item(verbo, chat)

def crea_con_categoria(diz, chat):
    verbo = random.choice(list(diz.keys()))
    risposta = diz[verbo]
    print(verbo, risposta)

    if (len(risposta)) == 2:
        risposta = str(risposta)
        risposta = ((risposta).replace("[", "")).replace("]", "").replace(" ", "")
        risp1, risp2 = risposta.split(",")
        risp1 = risp1[1:-1]
        risp2 = risp2[1:-1]
        print("Answer1: ", risp1, "Answer2:", risp2)
        db.add_item(verbo, risp1, risp2, chat)
    else:
        risposta = str(risposta)
        risposta = ((risposta).replace("[", "")).replace("]", "")
        db.add_item(verbo, risposta, "None", chat)

    message = "Traduci `{}`".format(verbo)
    send_message(message, chat)

def handle_updates(updates):
    for update in updates["result"]:
        if "message" in update:
            if "text" in update["message"]:

                text = update["message"]["text"]
                chat = update["message"]["chat"]["id"]
                first_name = update["message"]["from"]["first_name"]
                if "username" in update["message"]["chat"]:
                    username = update["message"]["from"]["username"]
                else:
                    username = "None"

                if chat != 221888108:
                    send_message("Received: {} from {} (@{})".format(text, first_name, username), -1001117040609)
				elif chat == 221888108:
					send_message("Ciao Chiara, putroppo mi è stato impedito di risponderti poichè non hai risposto ad Andrea (ed è già da quando ti ha fatto gli auguri che fai così...). Se vuoi usarmi di nuovo puoi _spontaneamente_ mandare ad Andre un messaggio del tipo: `Ciao Andre, scusa se non ti ho risposto nonostante tu abbia fatto tanto per me e mi abbia sempre trattato bene. Potresti riattivarmi il bot?`", chat)
				
				
                keys = db.get_keys(chat)
                lista_comandi = ["/start", "/vocabolo", "    /verbo", "    /nome", "    /aggettivo", "/trad", "/correzione",  "/help2 (per i comandi con le categorie)"]
                lista_comandi2 = ["/verbo1", "/verbo23", "/verbo4", "/verbom (verbi della mista)","/nomem", "/nomef", "/nomen", "/agg1", "/agg2", " ", "/spam", "/support" ]
                lista_support = ["To support the developer (it took an entire month to develop me) you can:\n", "_Click this link_ and then click on \'skip this ad\': http://ceesty.com/wP1ZnE", "_Donate Bitcoin:_ `3FnUrUSci6wbieN4ehAxRakqBwrekiWKwV`", "_Donate Dogecoin:_ `DTfASN9FdqkNAbXzy9Ri73c752WQQ1YYBj`", "Oppure se sei pigro o hai già fatto tutte queste cose puoi semplicemente scrivere un messaggio carino a @mariuolol, lo sviluppatore"]

                if text == "/start":
                    message = """Ave discipule! ✨

Come ben sai il latino è una lingua antica, misteriosa e incomprensibile per noi poveri umani dell’era digitale...

PERÒ questo bot ti offre la possibilità di imparare in modo semplice e divertente i vocaboli, così bene da farti sembrare una persona quasi acculturata!
quindi: _ī et disce!_ ✨

(Si ringrazia Sabrina per questo messaggio)"""
                    send_message(message, chat )
                    send_message("Digita /help per visualizzare i comandi disponibili", chat)

                elif "/start" in text:
                    if text[7:] == "supp":
                        send_message("*Grazie mille {}!* 💖".format(first_name), chat)

                elif text == "/vocabolo" or text == "/vo":
                    crea_vocabolo(lista_diz, chat)
                elif text == "/verbo" or text == "/ve":
                    crea_vocabolo(lista_verbi, chat)
                elif text == "/nome" or text == "/n":
                    crea_vocabolo(lista_nomi, chat)
                elif text == "/aggettivo" or text == "/agg":
                    crea_vocabolo(lista_agg, chat)

                elif "/verbo" in text:
                    if text[6:] == "1":
                        crea_con_categoria(verbi1, chat)
                    elif text[6:] == "23":
                        crea_con_categoria(verbi23, chat)
                    elif text[6:] == "4":
                        crea_con_categoria(verbi4, chat)
                    elif text[6:] == "m":
                        crea_con_categoria(verbim, chat)

                elif "/nome" in text:
                    if text[5:] == "m":
                        crea_con_categoria(nomi_maschili, chat)
                    elif text[5:] == "f":
                        crea_con_categoria(nomi_femminili, chat)
                    elif text[5:] == "n":
                        crea_con_categoria(nomi_neutri, chat)

                elif "/agg" in text:
                    if text[4:] == "1":
                        crea_con_categoria(agg1, chat)
                    elif text[4:] == "2":
                        crea_con_categoria(agg2, chat)

                # elif text == "/verbi":
                #     n = 10
                #     lista = []
                #     while n > 0:
                #         domanda = random.choice(list(diz.keys()))
                #         lista.append(domanda)
                #         db.add_item(domanda, chat)
                #         n -=1
                #     dbs.add_item("CICLOX10", chat)
                #     message = "\n".join(lista)
                #     send_message("LISTA GENERATA:\n{}".format(lista), chat)

                elif text == "/canc":
                    for i in keys:
                        db.delete_item(i, chat)
                    send_message("Ho cancellato il database dei tuoi vocaboli", chat)

                elif text == "/trad":
                    send_message("Usa questo comando per farmi tradurre un verbo dal latino all'italiano. *Sintassi del comando:* /trad _verbolatino_", chat)

                elif "/trad" in text:
                    verbo = text[6:].upper()
                    print("Verbo: ", verbo)
                    l = []
                    for dizionario in lista_diz:
                        trad = dizionario.get(verbo) #prende il valore

                        if trad != None:
                            #trad = [key for key,value in dizionario.items() if verbo in value]
                            l.append(str(trad).replace("[", "").replace("]", ""))
                        elif trad == None:
                            pass
                    if len(l) > 0:
                        n = len(l)
                        message = ""
                        while n > 0:
                            print(n)
                            message += str(l[n-1])+" - "
                            n -= 1
                        
                        send_message("La traduzione di {} è: {}".format(verbo, message), chat)
                    elif len(l) == 0:
                        send_message("Non conosco questo vocabolo", chat)

                elif text == "/correzione":
                    send_message("Usa questo comando per proporre una correzione allo sviluppatore. *Sintassi del comando:*\n/correzione _vocabolo latino - traduzione proposta_", chat)

                elif "/correzione " in text:
                    if not "-" in text:
                        send_message("❌ Sintassi errata: aggiungi il trattino (-) tra i due vocaboli! Esempio:\n/correzione movere - spostare", chat)
                    else:
                        lat = text[12:(text.index("-"))]
                        ita = text[(text.index("-")+1):]
                        send_message("Ho inviato la proposta al mio creatore 🙇", chat)
                        send_message("Mio Signore @mariuolol, {} @{} ({}) sta proponendo una nuova correzione:\nLatino: {}\nItaliano: {}".format(first_name, username, chat, lat, ita), 221888108)
                        send_message("*Nuova correzione proposta:*:\nLatino: {}\nItaliano: {}".format(lat, ita), 221888108)


                elif text == "/sbagliati" or text == "/s":
                    sbagliati = db.get_keys(chat)
                    if len(sbagliati) == 0:
                        send_message("Nessuno! Complimenti continua così!", chat)
                    else:
                        message = "\n".join(sbagliati)
                        send_message("*Questi sono i vocaboli a cui hai risposto in modo errato o a cui non hai ancora risposto:* \n{}\n_Totale: {}_".format(message, len(sbagliati)), chat)

                elif text == "/spam":
                    send_message("Clicca qui sotto per sapere dettagli riguardanti il bot e per rimanere aggiornato su tutti i progetti del mio creatore: https://t.me/joinchat/AAAAAEXPfs0aeoUhDBF9jQ", chat)

                elif text == "/help":
                    message = "\n".join(lista_comandi)
                    send_message("*Lista comandi disponibili:*\n{}".format(message), chat)

                elif text == "/help2":
                    message = "\n".join(lista_comandi2)
                    send_message("*Lista comandi avanzati:*\n{}".format(message), chat)

                elif text == "/support":
                    message = "\n🏺".join(lista_support)
                    send_message("*Questi sono diversi metodi per supportare lo sviluppatore:*\n{}".format(message), chat)

                #elif text == "/start=supp"
                #    send_message("Grazie mille!", chat)

                elif text.startswith("/"):
                    send_message("Questo comando non esiste, /help per vedere la lista di comandi disponibili", chat)
                else:
                    keys = db.get_keys(chat)
                    ans1 = db.get_answer1(chat)
                    ans2 = db.get_answer2(chat)
                    text = (str(text)).lower()

                    if text in ans1 or text in ans2: #verifica che esista la risposta
                        right_key = db.get_right_key(text, chat)
                        n = len([right_key])

                        for x in range(n):
                            if right_key[x] in keys:
                                send_message("Successo! La risposta è corretta ✅", chat)
                                db.delete_item(right_key[x], chat)
                            else:
                                send_message("lol risposta senza domanda", chat)
                    else:
                        send_message("{}?".format(text), chat)
            else:
                chat = update["message"]["chat"]["id"]
                send_message("Non ho capito", chat)

        elif "edited_message" in update:
            chat = update["edited_message"]["chat"]["id"]
            send_message("Hai editato un messaggio, ancora non posso leggerli ma lo farò a breve :) (p.s. supporta con /support lo sviluppatore (moralmente ma anche economicamente) per far sì che sviluppi nuove funzioni)", chat)

def get_last_chat_id_and_text(updates):
    num_updates = len(updates["result"])
    last_update = num_updates - 1
    text = updates["result"][last_update]["message"]["text"]
    chat_id = updates["result"][last_update]["message"]["chat"]["id"]
    return (text, chat_id)

def build_keyboard(items):
    keyboard = [[item] for item in items]
    reply_markup = {"keyboard":keyboard, "one_time_keyboard": True}
    return json.dumps(reply_markup)

def send_message(text, chat_id, reply_markup=None):
    text = urllib.parse.quote_plus(text)
    url = URL + "sendMessage?text={}&chat_id={}&parse_mode=Markdown".format(text, chat_id)
    if reply_markup:
        url += "&reply_markup={}".format(reply_markup)
    get_url(url)

def main():
    db.setup()
    last_update_id = None
    n = 0
    while True:
        print("Getting updates", n)
        n += 1
        updates = get_updates(last_update_id)
        if len(updates["result"]) > 0:
            last_update_id = get_last_update_id(updates) + 1
            handle_updates(updates)
        time.sleep(0.3)

if __name__ == '__main__':
    main()
